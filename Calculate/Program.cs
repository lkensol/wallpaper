﻿using System;

namespace Calculate
{
    class Program
    {
        static void Main(string[] args)
        {
            //Параметры рулона

            Console.WriteLine("Введите длинну рулона, м: ");
            double lenghtRool = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите ширину рулона, м: ");
            double widthRool = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите раппорт рулона, м: ");
            double rapport = Convert.ToDouble(Console.ReadLine());


            //Параметры комнаты


            Console.WriteLine("Введите длинну комнаты, м: ");
            double lenghtRoom = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите ширину комнаты, м: ");
            double widthRoom = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите высоту комнаты, м: ");
            double heightRoom = Convert.ToDouble(Console.ReadLine());

            double squareRoom = 0;

            //Расчет

            double remainder = heightRoom%rapport;

            //Проверка равен ли раппорт высоте комнаты
            if (remainder == 0)
            {
                squareRoom = 2 * heightRoom * (lenghtRoom + widthRoom);
            }
            else if (remainder != 0) {
                squareRoom = 2 * (heightRoom + rapport) * (lenghtRoom + widthRoom);
            }

            double amountRool = squareRoom / (lenghtRool * widthRool);
            double numberRools = Math.Ceiling(amountRool);

            double squareRools = numberRools * lenghtRool * widthRool;
            double percentRemainder = (100-Math.Round(((squareRoom / squareRools) * 100), 2));
            double metrRemainder = (squareRools - squareRoom) / widthRool;

            Console.WriteLine("Необходимое количество рулонов: {0}. Осталось {1}%, или {2} м", numberRools, percentRemainder, metrRemainder);
            
        }
    }
}
